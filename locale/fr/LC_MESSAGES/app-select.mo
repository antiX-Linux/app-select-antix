��            )         �     �     �     �     �                9     =     \  
   y     �     �     �     �     �  	   �     �     �     �          
  	        $  	   )     3     F     R     ^     p     �  �  �  #   7     [  '   u  &   �     �  7   �       1     6   P     �     �     �     �     �     �     �          .     >     [  ,   `  	   �     �     �     �     �     �     �     	     &	                                                                                          	                          
                    
Displaying filtered results
   Display Columns    App Select  : Custom User File : Personal Menu File : could not run custom command
  All Cannot generate pixbuf from %s Cannot get filename for icon Categories Categories Only Configure Mime Types Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Local User Apps Mime Type Name Name Only Personal Menu Apps Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: Wallon Wallon, 2023
Language-Team: French (https://app.transifex.com/anticapitalista/teams/10162/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 
Affichage des résultats filtrés
   Afficher les colonnes    Chercher une application - App Select  : Fichier personnel de l’utilisateur : Fichier du menu personnel : impossible d’exécuter la commande personnalisée
  Tous  Impossible de générer un pixbuf à partir de %s Impossible d’obtenir le nom du fichier de l’icône Catégories Seulement les catégories Configurer les types Mime Description Seulement la description Exécutable Seulement les exécutables La mise à jour a échoué Nom générique Seulement le nom générique Info Applications personnelles de l’utilisateur Type Mime Nom Seulement le nom Applications du menu personnel Recharger la liste Exécuter un programme Rechercher / Filtrer :  Mise à jour effectuée Tapez pour filtrer... 