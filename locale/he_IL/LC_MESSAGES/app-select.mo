��          �      �           	     '     ;      H     i  
   m     x     �     �     �  	   �     �     �     �     �     �  	   �     �               "     7  �  I  0   ;     l     �  ?   �     �     �     �  
             +     ?     \     r     �     �     �     �     �     �     �     �                                 
                                                                        	    
Displaying filtered results
   Display Columns    App Select  : could not run custom command
  All Categories Categories Only Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Name Name Only Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>, 2023
Language-Team: Hebrew (Israel) (https://www.transifex.com/anticapitalista/teams/10162/he_IL/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: he_IL
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n == 2 && n % 1 == 0) ? 1: (n % 10 == 0 && n % 1 == 0 && n > 10) ? 2 : 3;
 
התוצאות המסוננות מופיעות
 הצגת עמודות בחירת יישום : לא ניתן להריץ פקודה בהתאמה אישית
  הכול קטגוריות קטגוריות בלבד תיאור תיאור בלבד קובץ הפעלה קובץ הפעלה בלבד העדכון נכשל שם כללי שם כללי בלבד מידע שם שם בלבד רעיון רשימה הרצת תוכנית חיפוש / יסינון: עודכן בהצלחה הקלדה מסננת… 