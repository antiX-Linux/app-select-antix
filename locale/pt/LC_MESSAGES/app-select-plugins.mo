��    	      d      �       �      �       �        5   9  4   o     �     �  	   �  �  �     u  #   �     �  *   �  :         ;     S     l                                       	       + Send app to Personal menu + Send icon to the (zzz) Desktop + Send icon to the toolbar - Remove this app from Personal Menu (if it is there) - Remove this icon from the toolbar (if it is there) Open in add-desktop Open in text editor View icon Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-07-05 16:37+0000
Last-Translator: Paulo C., 2023
Language-Team: Portuguese (https://app.transifex.com/anticapitalista/teams/10162/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 + Adicionar ao Menu Pessoal + Adicionar ao Ambiente de Trabalho + Adicionar à barra de tarefas - Remover do Menu Pessoal (se lá estiver) - Remover este ícone da barra de tarefas (se lá estiver) Abrir com o add-desktop Abrir no Editor de Texto Ver o ícone 