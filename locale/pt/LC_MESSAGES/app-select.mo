��            )         �     �     �     �     �                9     =     \  
   y     �     �     �     �     �  	   �     �     �     �          
  	        $  	   )     3     F     R     ^     p     �  �  �  #   6     Z     l  "   �     �  7   �     �  &      ,   '  
   T     _     q     �     �     �     �     �     �     �     �     
     $     <     A     M     b     s     �     �     �                                                                                          	                          
                    
Displaying filtered results
   Display Columns    App Select  : Custom User File : Personal Menu File : could not run custom command
  All Cannot generate pixbuf from %s Cannot get filename for icon Categories Categories Only Configure Mime Types Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Local User Apps Mime Type Name Name Only Personal Menu Apps Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: Paulo C., 2023
Language-Team: Portuguese (https://app.transifex.com/anticapitalista/teams/10162/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 
A mostrar resultados da filtragem
 Mostrar colunas   Seleção de aplicações Ficheiro customizado do Utilizador Ficheiro do Menu Pessoal : não foi possível executar o comando personalizado
  Todos Impossivel gerar pixbuf a partir de %s Impossível obter nome do ficheiro do ícone Categorias Apenas categorias Configurar Tipos Mime Descrição Apenas descrição Executável Apenas executável Falha ao atualizar Nome genérico Apenas nome genérico Informação Apps locais do Utilizador Tipo de ficheiro (MIME) Nome Apenas nome Apps do Menu Pessoal Recarregar lista Executar programa Pesquisar / Filtrar:  Atualizado com êxito Escreva para filtrar... 