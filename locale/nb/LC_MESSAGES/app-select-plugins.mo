��    	      d      �       �      �       �        5   9  4   o     �     �  	   �  �  �  !   a      �     �  -   �  )   �          .     C                                       	       + Send app to Personal menu + Send icon to the (zzz) Desktop + Send icon to the toolbar - Remove this app from Personal Menu (if it is there) - Remove this icon from the toolbar (if it is there) Open in add-desktop Open in text editor View icon Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-07-05 16:37+0000
Last-Translator: heskjestad <cato@heskjestad.xyz>, 2023
Language-Team: Norwegian Bokmål (https://app.transifex.com/anticapitalista/teams/10162/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 + Send program til personlig-meny + Send ikon til (zzz) skrivebord + Send ikon til verktøylinje - Fjern program fra personlig-meny (hvis der) - Fjern ikon fra verktøylinje (hvis der) Åpne i add-desktop Åpne i tekstprogram Vis ikon 