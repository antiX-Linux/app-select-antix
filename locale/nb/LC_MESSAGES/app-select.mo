��            )         �     �     �     �     �                9     =     \  
   y     �     �     �     �     �  	   �     �     �     �          
  	        $  	   )     3     F     R     ^     p     �  �  �     "     ?     L     Y     o  '   �     �     �  $   �  
   �     �               +     ;     D     R     h     v     �     �  	   �     �     �     �     �     �     �          !                                                                                          	                          
                    
Displaying filtered results
   Display Columns    App Select  : Custom User File : Personal Menu File : could not run custom command
  All Cannot generate pixbuf from %s Cannot get filename for icon Categories Categories Only Configure Mime Types Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Local User Apps Mime Type Name Name Only Personal Menu Apps Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: heskjestad <cato@heskjestad.xyz>, 2023
Language-Team: Norwegian Bokmål (https://app.transifex.com/anticapitalista/teams/10162/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 
Viser filtrerte resultater
 Vis kolonner Velg program : Tilpasset brukerfil : Personlig menyfil : klarte ikke kjøre selvvalgt kommando Alle Kan ikke lage pixbuf fra %s Klarte ikke hente filnavn for ikonet Kategorier Kun kategorier Sett opp MIME-typer Beskrivelse Kun beskrivelse Kjørbar Kun kjørbare Klarte ikke oppdatere Generisk navn Kun generisk navn Info Programmer lokal bruker MIME-type Navn Kun navn Programmer personlig meny Last inn liste på ny Kjør program Søk / filtrer: Oppdatering fullført Skriv for å filtrere … 