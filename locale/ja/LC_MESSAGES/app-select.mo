��            )         �     �     �     �     �                9     =     \  
   y     �     �     �     �     �  	   �     �     �     �          
  	        $  	   )     3     F     R     ^     p     �  y  �  )        ;  !   N  )   p  #   �  5   �  	   �  )   �  6   (     _     l          �     �     �     �  '   �     �     �     
  $        6     E     L     Y     x     �     �     �  '   �                                                                                          	                          
                    
Displaying filtered results
   Display Columns    App Select  : Custom User File : Personal Menu File : could not run custom command
  All Cannot generate pixbuf from %s Cannot get filename for icon Categories Categories Only Configure Mime Types Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Local User Apps Mime Type Name Name Only Personal Menu Apps Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: Green <usergreen@users.osdn.me>, 2023
Language-Team: Japanese (https://app.transifex.com/anticapitalista/teams/10162/ja/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ja
Plural-Forms: nplurals=1; plural=0;
 
フィルタリングの結果を表示
 コラムの表示 アプリケーションの選択 : カスタムユーザーのファイル : 個人メニューのファイル : カスタムなコマンドは実行できません すべて %s から pixbuf が生成できません アイコンのファイル名を取得できません カテゴリ カテゴリのみ Mime タイプの設定 説明 説明のみ 実行 実行のみ アップデートに失敗しました 一般的な名前 一般名のみ 情報 ローカルユーザーのアプリ Mime タイプ 名前 名前のみ 個人メニューのアプリ 一覧の再読込み プログラムの実行 検索 / フィルタ:  アップデート成功です タイプしてフィルタリング... 