��            )         �     �     �     �     �                9     =     \  
   y     �     �     �     �     �  	   �     �     �     �          
  	        $  	   )     3     F     R     ^     p     �  �  �     $     C     T  "   l     �  .   �     �  #   �  '        ,     8     I     _     l     ~  	   �     �     �     �     �      �  	   �     �     �          *     9     N     `     {                                                                                          	                          
                    
Displaying filtered results
   Display Columns    App Select  : Custom User File : Personal Menu File : could not run custom command
  All Cannot generate pixbuf from %s Cannot get filename for icon Categories Categories Only Configure Mime Types Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Local User Apps Mime Type Name Name Only Personal Menu Apps Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: Amigo, 2023
Language-Team: Spanish (https://app.transifex.com/anticapitalista/teams/10162/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 
Mostrar resultados filtrados
 Mostrar columnas Seleccionar aplicación : Archivo de usuario personalizado : Archivo de menú personal : no se pudo ejecutar el comando personalizado Todos No se puede generar pixbuf desde %s No se puede obtener el nombre del icono Categorías Solo categorías Configurar tipos mime Descripción Solo descripción Exec Solo exec Fallo al actualizar Nombre genérico Solo nombre genérico Info Aplicaciones de usuarios locales Tipo mime Nombre Solo nombre Aplicaciones de menú personal Recargar lista Ejecutar el programa Buscar / Filtrar: Se actualizó exitosamente Escriba para filtrar... 