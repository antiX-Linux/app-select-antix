��    	      d      �       �      �       �        5   9  4   o     �     �  	   �  �  �  &   {  "   �  )   �  :   �  =   *     h     �  	   �                                       	       + Send app to Personal menu + Send icon to the (zzz) Desktop + Send icon to the toolbar - Remove this app from Personal Menu (if it is there) - Remove this icon from the toolbar (if it is there) Open in add-desktop Open in text editor View icon Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-07-05 16:37+0000
Last-Translator: Manuel <senpai99@hotmail.com>, 2023
Language-Team: Spanish (https://app.transifex.com/anticapitalista/teams/10162/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 + Enviar aplicación al Menú personal + Enviar icono al Escritorio (zzz) + Enviar icono a la barra de herramientas - Quitar la aplicación del Menú personal (si está ahí) - Quitar el icono de la barra de herramientas (si está ahí) Abrir en añadir-escritorio Abrir en editor de texto Ver icono 