��          �      �           	     '     ;      H     i  
   m     x     �     �     �  	   �     �     �     �     �     �  	   �     �               "     7  �  I  E   C  '   �     �  Z   �     ,     5     H     h     y     �  #   �  $   �     �  (        8     M     T  '   h     �     �  !   �  '   �                            
                                                                        	    
Displaying filtered results
   Display Columns    App Select  : could not run custom command
  All Categories Categories Only Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Name Name Only Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: Andrei Stepanov, 2023
Language-Team: Russian (https://app.transifex.com/anticapitalista/teams/10162/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 
Отображаются результаты для фильтра
 Отображение столбцов Выбор приложения : не удалось запустить пользовательскую команду
  Всех Категории Только категории Описание Только описание Выполнить Только исполняемый Не удалось обновить Общее название Только общее название Информация Имя Только имя Перезагрузить список Запуск программы Поиск / фильтр:  Успешно обновлено Введите для фильтра… 