��          �      �           	     '     ;      H     i  
   m     x     �     �     �  	   �     �     �     �     �     �  	   �     �               "     7  n  I  "   �     �     �  1   �     .  
   5     @     P     W     l     t     �     �     �     �     �     �     �     �     	          +                            
                                                                        	    
Displaying filtered results
   Display Columns    App Select  : could not run custom command
  All Categories Categories Only Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Name Name Only Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: Risto Pärkkä, 2023
Language-Team: Finnish (https://www.transifex.com/anticapitalista/teams/10162/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
 
Näytetään suodatetut tulokset
 Näytä sarakkeet Ohjelmavalinta : ei voinut suorittaa räätälöityä komentoa
  Kaikki Kategoriat Vain kategoriat Kuvaus Pelkästään Kuvaus Suorita Suorita Ainoastaan Päivitys epäonnistui Yleinen Nimi Yleinen Nimi Ainoastaan Tiedot Nimi Pelkästään Nimi Lataa lista uudelleen Suorita sovellus Hae / suodata: Päivitys onnistui Kirjoita suodattaaksesi... 