��    	      d      �       �      �       �        5   9  4   o     �     �  	   �  n  �  0   E      v  "   �  J   �  ;        A     V     l                                       	       + Send app to Personal menu + Send icon to the (zzz) Desktop + Send icon to the toolbar - Remove this app from Personal Menu (if it is there) - Remove this icon from the toolbar (if it is there) Open in add-desktop Open in text editor View icon Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-07-05 16:37+0000
Last-Translator: Risto Pärkkä, 2023
Language-Team: Finnish (https://app.transifex.com/anticapitalista/teams/10162/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
 + Lähetä sovellus henkilökohtaiseen valikkoon + Lähetä kuvake työpöydälle + Lähetä kuvake työkalupalkkiin - Poista tämä sovellus henkilökohtaisesta valikosta (jos se on siellä) - Poista tämä kuvake työkalupalkista (jos se on siellä) Avaa add-desktopissa Avaa tekstieditorissa Näytä kuvake 