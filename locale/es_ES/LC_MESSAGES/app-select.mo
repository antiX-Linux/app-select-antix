��            )         �     �     �     �     �                9     =     \  
   y     �     �     �     �     �  	   �     �     �     �          
  	        $  	   )     3     F     R     ^     p     �  �  �     J     i     z  "   �     �  .   �        #     '   *     R     ^     o     �     �     �  	   �     �     �     �     �      �  	        )     0     <     \     k     �     �     �                                                                                          	                          
                    
Displaying filtered results
   Display Columns    App Select  : Custom User File : Personal Menu File : could not run custom command
  All Cannot generate pixbuf from %s Cannot get filename for icon Categories Categories Only Configure Mime Types Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Local User Apps Mime Type Name Name Only Personal Menu Apps Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: Manuel <senpai99@hotmail.com>, 2023
Language-Team: Spanish (Spain) (https://app.transifex.com/anticapitalista/teams/10162/es_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es_ES
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 
Mostrar resultados filtrados
 Mostrar columnas Seleccionar aplicación : Archivo de usuario personalizado : Archivo de menú personal : no se pudo ejecutar el comando personalizado Todos No se puede generar pixbuf desde %s No se puede obtener el nombre del icono Categorías Solo categorías Configurar tipos mime Descripción Solo descripción Exec Solo exec Error de actualización Nombre genérico Solo nombre genérico Informacion Aplicaciones de usuarios locales Tipo mime Nombre Solo nombre Aplicaciones del menú personal Recargar lista Ejecutar el programa Buscar / Filtrar: Se actualizó exitosamente Escriba para filtrar... 