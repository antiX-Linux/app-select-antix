��            )         �     �     �     �     �                9     =     \  
   y     �     �     �     �     �  	   �     �     �     �          
  	        $  	   )     3     F     R     ^     p     �  �  �  $   F     k     z     �     �  -   �     �  !   �  1     	   C     M     \     p     |  
   �     �     �     �     �     �     �     �     
  	             1     A     R     `     x                                                                                          	                          
                    
Displaying filtered results
   Display Columns    App Select  : Custom User File : Personal Menu File : could not run custom command
  All Cannot generate pixbuf from %s Cannot get filename for icon Categories Categories Only Configure Mime Types Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Local User Apps Mime Type Name Name Only Personal Menu Apps Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: Dede Carli <dede.carli.drums@gmail.com>, 2023
Language-Team: Italian (https://app.transifex.com/anticapitalista/teams/10162/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 
Visualizzazione risultati filtrati
 Mostra colonne Seleziona app : File utente personalizzato : File menu personale : non posso eseguire comandi personalizzati
  Tutti Impossibile generare pixbuf da %s Impossibile ottenere il nome del file per l'icona Categorie Solo categorie Configura tipi mime Descrizione Solo descrizione Eseguibile Solo eseguibile Impossibile aggiornare Nome generico Solo nome generico Info Apps dell'utente locale Tipo di mime Nome Solo nome Apps del menu personale Ricarica elenco Esegui programma Cerca/filtra: Aggiornato con successo Digita per filtrare... 