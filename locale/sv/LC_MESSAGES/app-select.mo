��          �   %   �      `     a          �      �     �     �     �  
                  1     =     N  	   S     ]     n     {     �  	   �     �  	   �     �     �     �     �     �  �  �     �     �  	   �  &   �     �  !   �        
   $     /     A     X     d     w     |     �     �     �     �     �     �     �     �     �               &                                                      	                                                        
              
Displaying filtered results
   Display Columns    App Select  : could not run custom command
  All Cannot generate pixbuf from %s Cannot get filename for icon Categories Categories Only Configure Mime Types Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Mime Type Name Name Only Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: Henry Oquist <henryoquist@nomalm.se>, 2023
Language-Team: Swedish (https://app.transifex.com/anticapitalista/teams/10162/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
 
Visar filtrerade resultat
 Visa Kolumner Välj App : kunde inte köra anpassat kommando
  Alla Kan inte generera pixbuf från %s Kan inte hitta filnamn för ikon Kategorier Enbart Kategorier Konfigurera Mime-typer Beskrivning Enbart Beskrivning Exec Enbart Exec Misslyckades med uppdatering Allmänt Namn Enbart Allmänt Namn Info Mime-typ Namn Enbart Namn Ladda om Lista Kör Program Sök / Filter:  Lyckad uppdatering Typ att filtrera... 