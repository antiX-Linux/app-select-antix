��    	      d      �       �      �       �        5   9  4   o     �     �  	   �  �  �     [  !   {  !   �  :   �  <   �     7     L     g                                       	       + Send app to Personal menu + Send icon to the (zzz) Desktop + Send icon to the toolbar - Remove this app from Personal Menu (if it is there) - Remove this icon from the toolbar (if it is there) Open in add-desktop Open in text editor View icon Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-07-05 16:37+0000
Last-Translator: Henry Oquist <henryoquist@nomalm.se>, 2023
Language-Team: Swedish (https://app.transifex.com/anticapitalista/teams/10162/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
 + Sänd app till personlig meny + Sänd ikon till (zzz) Skrivbord + Sänd ikon till verktygsfältet - Ta bort denna app från personlig meny (om den är där) - Ta bort denna ikon från verktygsfältet (om den är där) Öppna i add-desktop Öppna i en textredigerare Se ikon 