��    	      d      �       �      �       �        5   9  4   o     �     �  	   �  �  �  J   a  [   �  N     w   W  w   �  N   G  R   �  #   �                                       	       + Send app to Personal menu + Send icon to the (zzz) Desktop + Send icon to the toolbar - Remove this app from Personal Menu (if it is there) - Remove this icon from the toolbar (if it is there) Open in add-desktop Open in text editor View icon Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-07-05 16:37+0000
Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2023
Language-Team: Greek (https://app.transifex.com/anticapitalista/teams/10162/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
 + Αποστολή εφαρμογής στο Προσωπικό μενού + Αποστολή εικονιδίου στην επιφάνεια εργασίας (zzz). + Αποστολή εικονιδίου στη γραμμή εργαλείων - Αφαιρέστε αυτήν την εφαρμογή από το Προσωπικό μενού (αν υπάρχει) - Αφαιρέστε αυτό το εικονίδιο από τη γραμμή εργαλείων (αν υπάρχει) Άνοιγμα στην προσθήκη επιφάνειας εργασίας Άνοιγμα στο πρόγραμμα επεξεργασίας κειμένου Προβολή εικονιδίου 