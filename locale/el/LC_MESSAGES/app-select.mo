��            )         �     �     �     �     �                9     =     \  
   y     �     �     �     �     �  	   �     �     �     �          
  	        $  	   )     3     F     R     ^     p     �  �  �  J   "     m  !   �  6   �  .   �  e        y  G   �  i   �     4     I  9   g     �     �     �     �  $   �     	       	     A	  0   X	     �	  
   �	     �	  2   �	  %   �	  )   
  "   ;
  #   ^
  ,   �
                                                                                          	                          
                    
Displaying filtered results
   Display Columns    App Select  : Custom User File : Personal Menu File : could not run custom command
  All Cannot generate pixbuf from %s Cannot get filename for icon Categories Categories Only Configure Mime Types Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Local User Apps Mime Type Name Name Only Personal Menu Apps Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2023
Language-Team: Greek (https://app.transifex.com/anticapitalista/teams/10162/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
 
Εμφάνιση φιλτραρισμένων αποτελεσμάτων
 Εμφάνιση στηλών Επιλογή εφαρμογής : Προσαρμοσμένο αρχείο χρήστη : Αρχείο προσωπικού μενού : δεν ήταν δυνατή η εκτέλεση της προσαρμοσμένης εντολής Όλες Δεν είναι δυνατή η δημιουργία pixbuf από %s Δεν είναι δυνατή η λήψη ονόματος αρχείου για το εικονίδιο Κατηγορίες Μόνο κατηγορίες Διαμόρφωση τύπων συσχέτισης MIME Περιγραφή Μόνο Περιγραφή Exec Μόνο exec Η ενημέρωση απέτυχε Γενικό όνομα Μόνο γενικό όνομα Πληροφορίες Τοπικές εφαρμογές χρηστών Τύπος MIME Όνομα Μόνο όνομα Εφαρμογές προσωπικού μενού Επαναφόρτωση λίστας Εκτέλεση προγράμματος Αναζήτηση / Φίλτρο: Επιτυχής ενημέρωση Πληκτρολογήστε φίλτρο... 