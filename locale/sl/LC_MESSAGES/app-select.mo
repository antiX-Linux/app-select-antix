��          �   %   �      `     a          �      �     �     �     �  
                  1     =     N  	   S     ]     n     {     �  	   �     �  	   �     �     �     �     �     �  �  �     �     �     �  )   �     $     (  *   H  
   s     ~     �     �  
   �     �     �     �     �     �       
        (  	   ,     6     M     ]     o     �                                                      	                                                        
              
Displaying filtered results
   Display Columns    App Select  : could not run custom command
  All Cannot generate pixbuf from %s Cannot get filename for icon Categories Categories Only Configure Mime Types Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Mime Type Name Name Only Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: Arnold Marko <arnold.marko@gmail.com>, 2023
Language-Team: Slovenian (https://app.transifex.com/anticapitalista/teams/10162/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 
Prikaz filtriranih rezultatov
 Prikaz stolpcev Izbira aplikacije : ukaza po meri ni bilo mogoče izvesti
  Vse Ne morem ustvariti pixbuf iz %s Ne morem pridobiti imena datoteke za ikono Kategorije Zgolj kategorije Konfiguriranje MIME vrst Opis Zgolj opis Izvršna Zgolj izvršna Posodobitev ni bila uspešna Generično ime Zgolj generično ime Informacije Vrsta MIME Ime Zgolj ime Ponovno naloži seznam Zaženi program Iskanje / filter: Uspešno posodobljeno Tipkajte za filter... 