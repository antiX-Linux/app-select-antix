��          �   %   �      P     Q     o     �      �     �     �     �  
   �     �          !     -  	   >     H     Y     f     x  	   }     �  	   �     �     �     �     �     �  �  �  %   p     �     �  #   �     �  &   �  )        8     A     Q  
   g     r     �     �     �     �     �  
   �     �     �               $     8     Q                                                      	                                                         
              
Displaying filtered results
   Display Columns    App Select  : could not run custom command
  All Cannot generate pixbuf from %s Cannot get filename for icon Categories Categories Only Configure Mime Types Description Description Only Exec Only Failed to update Generic Name Generic Name Only Info Mime Type Name Name Only Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: Besnik Bleta <besnik@programeshqip.org>, 2023
Language-Team: Albanian (https://app.transifex.com/anticapitalista/teams/10162/sq/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sq
Plural-Forms: nplurals=2; plural=(n != 1);
 
Po shfaqen përfundime të filtruar
 Shfaq Shtylla Përzgjedhje Aplikacioni : s’u xhirua dot urdhër vetjak
  Krejt S’prodhohet dot pixbuf-i që prej %s S’merret dot emër kartele për ikonën Kategori Vetëm Kategori Formësoni Lloje MIME Përshkrim Vetëm Përshkrim Vetëm Exec S’u arrit të përditësohet Emër i Thjeshtë Vetëm Emër të Thjeshtë Hollësi Lloje MIME Emër Vetëm Emër Ringarko Listën Xhironi Program Kërkim / Filtër:  U përditësua me sukses Shtypni që të filtrohet… 