��    	      d      �       �      �       �        5   9  4   o     �     �  	   �  �  �  ,   _  $   �     �  B   �  3        E     Z     w                                       	       + Send app to Personal menu + Send icon to the (zzz) Desktop + Send icon to the toolbar - Remove this app from Personal Menu (if it is there) - Remove this icon from the toolbar (if it is there) Open in add-desktop Open in text editor View icon Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-07-05 16:37+0000
Last-Translator: Besnik Bleta <besnik@programeshqip.org>, 2023
Language-Team: Albanian (https://app.transifex.com/anticapitalista/teams/10162/sq/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sq
Plural-Forms: nplurals=2; plural=(n != 1);
 + Dërgoje aplikacionin te menuja Personalë + Dërgoje ikonën te Desktopi (zzz) + Dërgoje ikonën te paneli - Hiqe këtë aplikacion prej menusë Personalë (në qoftë atje) - Hiqe këtë ikonë prej panelit (në qoftë atje) Hape në add-desktop Hape në përpunues tekstesh Shiheni ikonën 