��    	      d      �       �      �       �        5   9  4   o     �     �  	   �  �  �  $   �  )   �  ,   �  6     >   U  !   �     �     �                                       	       + Send app to Personal menu + Send icon to the (zzz) Desktop + Send icon to the toolbar - Remove this app from Personal Menu (if it is there) - Remove this icon from the toolbar (if it is there) Open in add-desktop Open in text editor View icon Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-07-05 16:37+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2023
Language-Team: Portuguese (Brazil) (https://app.transifex.com/anticapitalista/teams/10162/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 + Adicionar o Ícone no Menu Pessoal + Adicionar o Ícone na Área de Trabalho + Adicionar o Ícone na Barra de Ferramentas - Remover Este Ícone do Menu Pessoal (se estiver lá) - Remover Este Ícone da Barra de Ferramentas (se estiver lá) Abrir o Arquivo ‘add-desktop’ Abrir com o Editor de Texto Visualizar o Ícone 