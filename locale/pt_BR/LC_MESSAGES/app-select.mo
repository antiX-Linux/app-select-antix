��            )         �     �     �     �     �                9     =     \  
   y     �     �     �     �     �  	   �     �     �     �          
  	        $  	   )     3     F     R     ^     p     �  �  �  $   c     �  .   �  #   �     �  7        C  :   I  8   �  
   �     �     �     �               (  "   @     c     r     �      �     �     �     �     �     �     	     "	  )   8	     b	                                                                                          	                          
                    
Displaying filtered results
   Display Columns    App Select  : Custom User File : Personal Menu File : could not run custom command
  All Cannot generate pixbuf from %s Cannot get filename for icon Categories Categories Only Configure Mime Types Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Local User Apps Mime Type Name Name Only Personal Menu Apps Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2023
Language-Team: Portuguese (Brazil) (https://app.transifex.com/anticapitalista/teams/10162/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 
Exibindo os resultados da pesquisa
   Exibir em Colunas    Pesquisar Aplicativos com o ‘App Select’  : Arquivo Personalizado do Usuário : Arquivo do Menu Pessoal : Não foi possível executar o comando personalizado
  Todos Não foi possível gerar os dados da imagem (pixbuf) de %s Não foi possível obter o nome do arquivo para o ícone Categorias Apenas pela Categoria Configurar o Tipo do Mime Descrição Apenas pela Descrição Executável Apenas pelo Executável Ocorreu uma falha na atualização Nome Genérico Apenas pelo Nome Genérico Informações Aplicativos Pessoais do Usuário Tipo do Mime Nome Apenas pelo Nome Aplicativos do Menu Pessoal Recarregar a Lista Executar o Programa Pesquisar / Filtrar:  A atualização foi realizada com sucesso Digite aqui para pesquisar... 