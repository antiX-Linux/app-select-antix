��    	      d      �       �      �       �        5   9  4   o     �     �  	   �  ~  �  &   U  %   |  #   �  5   �  2   �     /     C     ]                                       	       + Send app to Personal menu + Send icon to the (zzz) Desktop + Send icon to the toolbar - Remove this app from Personal Menu (if it is there) - Remove this icon from the toolbar (if it is there) Open in add-desktop Open in text editor View icon Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-07-05 16:37+0000
Last-Translator: Eduard Selma <selma@tinet.cat>, 2023
Language-Team: Catalan (https://app.transifex.com/anticapitalista/teams/10162/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
 + Envia l'aplicació al menú Personal + Envia la icona a l'Escriptori (zzz) + Envia la icona a la barra d'eines - Elimina l'aplicació del menú Personal (si hi és) - Elimina la icona de la barra d'eines (si hi és) Obre en add-desktop Obre en un editor de text Veure la icona 