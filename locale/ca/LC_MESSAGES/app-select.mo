��            )         �     �     �     �     �                9     =     \  
   y     �     �     �     �     �  	   �     �     �     �          
  	        $  	   )     3     F     R     ^     p     �  ~  �           7     G     Y     y  #   �     �     �  &   �               "     6     C     V     [     m     �     �     �     �  
   �     �  
   �     �          &     :     J     i                                                                                          	                          
                    
Displaying filtered results
   Display Columns    App Select  : Custom User File : Personal Menu File : could not run custom command
  All Cannot generate pixbuf from %s Cannot get filename for icon Categories Categories Only Configure Mime Types Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Local User Apps Mime Type Name Name Only Personal Menu Apps Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: Eduard Selma <selma@tinet.cat>, 2023
Language-Team: Catalan (https://app.transifex.com/anticapitalista/teams/10162/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
 
Mostra dels resultats filtrats
 Mostra columnes Tria l'aplicació : Fitxer personalitzat d'usuari : Fitxer de Menú Personal no puc executar l'ordre específica Tots  No puc generar el pixbuf de %s No trobo el nom de fitxer per la icona Categories  Només categories Configura tpus MIME Descripció  Només descripció Exec Només executable Ha fallat l'actualització Nom genèric Només nom genèric Informació Aplicacions Locals d'Usuari Tipus MIME Nom  Només nom Aplicacions del Menú Personal Torna a carregar la llista Executa el programa Cerca / Filtre: Actualització satisfactòria  Tipus a filtrar... 