��            )         �     �     �     �     �                9     =     \  
   y     �     �     �     �     �  	   �     �     �     �          
  	        $  	   )     3     F     R     ^     p     �  b  �  $   �          4     F     f  <   �     �     �  &   �  
             '     @     M     `     g     s     �     �     �  %   �     �     �  	   �  "        &     6     G  )   S  !   }                                                                                          	                          
                    
Displaying filtered results
   Display Columns    App Select  : Custom User File : Personal Menu File : could not run custom command
  All Cannot generate pixbuf from %s Cannot get filename for icon Categories Categories Only Configure Mime Types Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Local User Apps Mime Type Name Name Only Personal Menu Apps Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: aer, 2023
Language-Team: German (https://app.transifex.com/anticapitalista/teams/10162/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
 
Auswahlergebnisse werden angezeigt
   Spalten anzeigen    Programmauswahl  Datei des derzeitigen Benutzers Datei des persönlichen Menüs : Benutzerdefinierter Befehl kann nicht ausgeführt werden
  Alle pixbuf nicht erstellbar aus %s Dateiname des Symbols nicht verfügbar Kategorien Nur Kategorien Mime-Typen konfigurieren Beschreibung Nur Beschreibungen Befehl Nur Befehle Aktualisierung fehlgeschlagen Allgemeiner Name Nur allgemeiner Name Informationen Anwendungen des derzeitigen Benutzers Mime-Typ Name Nur Namen Anwendungen im persönlichen Menü Liste neu laden Programm starten Suchfilter: Aktualisierung erfolgreich abgeschlossen. Auswahlkriterien hier eingeben... 