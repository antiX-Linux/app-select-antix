��    	      d      �       �      �       �        5   9  4   o     �     �  	   �  b  �  /   9  +   i  %   �  H   �  :     4   ?     t     �                                       	       + Send app to Personal menu + Send icon to the (zzz) Desktop + Send icon to the toolbar - Remove this app from Personal Menu (if it is there) - Remove this icon from the toolbar (if it is there) Open in add-desktop Open in text editor View icon Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-07-05 16:37+0000
Last-Translator: aer, 2023
Language-Team: German (https://app.transifex.com/anticapitalista/teams/10162/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
 + Anwendung zum Persönlichen Menü hinzufügen + Symbol zur Arbeitsoberfläche hinzufügen + Symbol zur Symbolleiste hinzufügen - Anwendung aus dem Persönlichen Menü entfernen (falls dort vorhanden) - Symbol aus Symbolleiste entfernen (falls dort vorhanden) Mit der Anwendung 'Menüeintrag hinzufügen' öffnen Im Texteditor öffnen Symbol betrachten 