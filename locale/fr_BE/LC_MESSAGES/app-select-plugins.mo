��    	      d      �       �      �       �        5   9  4   o     �     �  	   �  �  �  +   �     �  +   �  F   �  F   D  ;   �  "   �     �                                       	       + Send app to Personal menu + Send icon to the (zzz) Desktop + Send icon to the toolbar - Remove this app from Personal Menu (if it is there) - Remove this icon from the toolbar (if it is there) Open in add-desktop Open in text editor View icon Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-07-05 16:37+0000
Last-Translator: Wallon Wallon, 2023
Language-Team: French (Belgium) (https://app.transifex.com/anticapitalista/teams/10162/fr_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_BE
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 + Ajouter l’application au menu personnel + Ajouter l’icône au bureau + Ajouter l’icône à la barre d’outils - Supprimer cette application du menu personnel (si elle s’y trouve) - Supprimer cette icône de la barre d’outils (si elle s’y trouve) Ouvrir dans « Ajouter un raccourci → fichier .desktop » Ouvrir dans l’éditeur de textes Afficher l’icône 