#!/bin/bash
### Check if user is Root, if not, pop up window asking for password, to run updater. If Cancelled, exit.
# adapted from  https://stackoverflow.com/questions/42875809/checking-sudo-in-bash-script-with-if-statements 
if [[ "$EUID" = 0 ]]; then
    echo $root_message
else
    gksudo "$title"
    if sudo true; then
        echo $root_message
    else
        echo $not_root_message
        yad $windowicon --center --width=250 --text-align=center  --text="$not_root_message" --title="$title" --image="/usr/share/icons/papirus-antix/48x48/emblems/emblem-rabbitvcs-obstructed.png" --button=' x '  --borders=5 --fixed
        exit 1
    fi
fi

#create links to the scripts, that app-select needs in order to use them
sudo ln -sr send_to_toolbar /usr/lib/app-select/plugins/
sudo ln -sr remove_from_toolbar  /usr/lib/app-select/plugins/
sudo ln -sr add-to-personal-menu /usr/lib/app-select/plugins/
sudo ln -sr remove_from_personal_menu /usr/lib/app-select/plugins/
sudo ln -sr add-to-desktop /usr/lib/app-select/plugins/

#add entries to the app-select configuration file, so they show up in app-select's contextual menu, if they are not already there

if  ! grep -F "send_to_toolbar" ~/.config/app-select.conf; then
echo  "+ Send icon to the toolbar|send_to_toolbar \"%p\" \"%n\" \"%i\" \"%e\"" >> ~/.config/app-select.conf
fi

if  ! grep -F "remove_from_toolbar" ~/.config/app-select.conf; then
echo  "- Remove this icon from the toolbar (if it is there)|remove_from_toolbar \"%p\" \"%n\" \"%i\" \"%e\"" >> ~/.config/app-select.conf
fi

if  ! grep -F "add-to-personal-menu " ~/.config/app-select.conf; then
echo  "+ Send app to Personal menu|add-to-personal-menu \"%p\" \"%n\" \"%i\" \"%e\"" >> ~/.config/app-select.conf
fi

if  ! grep -F "remove_from_personal_menu" ~/.config/app-select.conf; then
echo  "- Remove this app from Personal Menu (if it is there)|remove_from_personal_menu \"%p\" \"%n\" \"%i\" \"%e\"" >> ~/.config/app-select.conf
fi

if  ! grep -F "add-to-desktop" ~/.config/app-select.conf; then
echo  "+ Send icon to the (zzz) Desktop|add-to-desktop \"%p\" \"%n\" \"%i\" \"%e\"" >> ~/.config/app-select.conf
fi
yad --center --title="app-select plugins" --text="Finished \n To use the plug-ins right click any application on app-select" --button="Ok"
